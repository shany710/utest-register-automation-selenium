package Tasks;

import Actions.Click;
import Actions.Escribir;
import Actions.PresionarTeclas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage4 {
    
    public static void completarInformacionForm4(
    		WebDriver driver,
            WebDriverWait wait,
            By contrasenia,
            By confirmContrasenia,
            By terminosUso,
            By politicasPrivacidad,
            By btnRegistrar,
            String s, 
            String s2) {
         	
    	Escribir.escribirTexto(wait, contrasenia, s2);
    	Escribir.escribirTexto(wait, confirmContrasenia, s2);
    	Click.checkearOpcion(driver, terminosUso);
    	Click.checkearOpcion(driver, politicasPrivacidad);
    	Click.darClic(wait, btnRegistrar);
    	Click.darClic(wait, btnRegistrar);
    	
    }
}

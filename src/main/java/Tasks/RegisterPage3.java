package Tasks;

import Actions.Seleccionar;
import UI.Register3UI;
import Actions.Click;
import Actions.PresionarTeclas;
import Actions.Escribir;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage3 {
    
    public static void completarInformacionForm3(
            WebDriver webDriver, 
            WebDriverWait wait,
            By version,
            By versionInput,
            By lenguaje,
            By lenguajeInput,
            By marcaDispositivo,
            By marcaDispositivoInput,
            By modeloDispositivo,
            By modeloDispositivoInput,
            By sistemaOp,
            By sistemaOpInput,
            By avanzar,
            String fVersion, 
            String fLenguaje, 
            String fMarcaDispositivo, 
            String fModeloDispositivo, 
            String fSistemaOpDispositivo) 
    {      
        Click.darClic(wait, version);
        Escribir.escribirTexto(wait, versionInput, fVersion);
        PresionarTeclas.presionarEnter(wait, versionInput);
        
        Click.darClic(wait, lenguaje);
        Escribir.escribirTexto(wait, lenguajeInput, fLenguaje);
        PresionarTeclas.presionarEnter(wait, lenguajeInput);
        
        Click.darClic(wait, marcaDispositivo);
        Click.darClic(wait, marcaDispositivoInput);
        Escribir.escribirTexto(wait, marcaDispositivoInput, fMarcaDispositivo);
        PresionarTeclas.presionarEnter(wait, marcaDispositivoInput);
        
        Click.darClic(wait, modeloDispositivo);
        Escribir.escribirTexto(wait, modeloDispositivoInput, fModeloDispositivo);
        PresionarTeclas.presionarEnter(wait, modeloDispositivoInput);
        
        Click.darClic(wait, sistemaOp);
        Escribir.escribirTexto(wait, sistemaOpInput, fSistemaOpDispositivo);
        PresionarTeclas.presionarEnter(wait, sistemaOpInput);
        
        Click.darClic(wait, avanzar);
       
        
    }
}

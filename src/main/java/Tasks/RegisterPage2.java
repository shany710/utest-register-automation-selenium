package Tasks;

import Actions.Click;
import Actions.PresionarTeclas;
import Actions.Escribir;
import Actions.Limpiar;
import Actions.Obtener;
import UI.Register2UI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage2 {
    
    private RegisterPage2(){}
    
    public static void completarInformacionForm2
    (
    		WebDriver driver, 
    		WebDriverWait wait,
    		By ciudad,
    		By codigoPostal,
    		By pais,
    		By avanzar,
    		String fCiudad,
    		String fCodigoPostal,
    		String fPais) 
    {
    		
    		String ciudadForm = Obtener.obtenerTexto(driver, ciudad);
    		String codigoPostalForm = Obtener.obtenerTexto(driver, codigoPostal);
    		String paisForm = Obtener.obtenerTexto(driver, pais);
    		
    		if(ciudadForm == null || ciudadForm.isEmpty())
    		{
    			Limpiar.limpiar(driver, ciudad);
    			Escribir.escribirTexto(wait,ciudad, fCiudad);
    			PresionarTeclas.presionarEnter(wait, ciudad);
    			
    		}
    		if(codigoPostalForm == null || codigoPostalForm.isEmpty())
    		{
    			Limpiar.limpiar(driver, codigoPostal);
    			Escribir.escribirTexto(wait, codigoPostal, fCodigoPostal);
    			PresionarTeclas.presionarEnter(wait, codigoPostal);
    		}
    		if(paisForm == null || paisForm.isEmpty()) {
    			
    			Limpiar.limpiar(driver, pais);
    			Escribir.escribirTexto(wait, pais, fPais);
    			PresionarTeclas.presionarEnter(wait, pais);
    		}
    		Click.darClic(wait, avanzar);
         
    }
    
}

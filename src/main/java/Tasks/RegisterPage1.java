package Tasks;

import Actions.Click;
import Actions.Escribir;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class RegisterPage1 {
      
    public static void completarInformacionForm1(
            WebDriverWait wait, 
            By primerNombre,
            By apellido,
            By email,
            By mes,
            By dia,
            By anio,
            By lenguajes,
            By avanzar,
            String tPrimerNombre,
            String tApellido,
            String tEmail,
            String tMes,
            String tDia,
            String tAnio,
            List<String> tLenguaje)
    {     
        
        Escribir.escribirTexto(wait, primerNombre, tPrimerNombre);
        Escribir.escribirTexto(wait, apellido, tApellido);
        Escribir.escribirTexto(wait, email, tEmail);
        Escribir.escribirTexto(wait, mes, tMes);
        Escribir.escribirTexto(wait, dia, tDia);
        Escribir.escribirTexto(wait, anio, tAnio);
        Escribir.escribirList(wait, lenguajes, tLenguaje);
        Click.darClic(wait, avanzar);
        
    }
    
}

package Actions;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Buscar {
	
	public static WebElement encontrarElemento(WebDriver driver, By locator) {
		return driver.findElement(locator);	
	}
	
	public static List<WebElement> encontrarElementos(WebDriver driver, By locator){
		return driver.findElements(locator);
	}
}

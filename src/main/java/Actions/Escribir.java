package Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Escribir {
    
    public static void escribirTexto(WebDriverWait wait, By locator, String texto){
        
    	wait.until(ExpectedConditions.elementToBeClickable(locator)).sendKeys(texto);
    }
    
    public static void escribirList(WebDriverWait wait, By locator, List<String> list) {
    	
    	for (String iterator : list) {
			
    		escribirTexto(wait, locator, iterator);
    	
		}
    }
    
}

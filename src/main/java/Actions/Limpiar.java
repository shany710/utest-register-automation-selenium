package Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Limpiar {

	public static void limpiar(WebDriver driver, By locator) {
		driver.findElement(locator).clear();
	}
	
}

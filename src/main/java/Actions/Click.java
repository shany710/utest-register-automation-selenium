package Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Click {
    
    public static void darClic(WebDriverWait wait, By locator){
    	
        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }
    
    public static void checkearOpcion(WebDriver driver,By locator) {
    	driver.findElement(locator).click();
    }
}

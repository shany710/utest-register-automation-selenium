package Actions;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Seleccionar {
    
    public static void seleccionarOpcionesSelect(WebElement elemento, String texto) {
		Select select= new Select(elemento);
		select.selectByVisibleText(texto);
    }
}

package Actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Obtener {
	
	public static String obtenerTexto(WebDriver driver, By locator) {
		return driver.findElement(locator).getText();
	}
	
	public static String obtenerTextoElemento(WebElement elemento) {
		return elemento.getText();
	}
}

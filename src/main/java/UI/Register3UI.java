package UI;

import org.openqa.selenium.By;

public class Register3UI {
    
    public static By version = By.name("osVersionId");
    public static By versionInput = By.xpath("//div[@name=\"osVersionId\"]/input");
    public static By lenguaje = By.name("osLanguageId");
    public static By lenguajeInput = By.xpath("//div[@name=\"osLanguageId\"]/input");
    public static By marcaDispositivo = By.name("handsetMakerId");
    public static By marcaDispositivoInput = By.xpath("//div[@name=\"handsetMakerId\"]/input");
    public static By modeloDispositivo = By.name("handsetModelId");
    public static By modeloDispInput = By.xpath("//div[@name=\"handsetModelId\"]/input");
    public static By sOperativo = By.name("handsetOSId");
    public static By sOperativoInput = By.xpath("//div[@name=\"handsetOSId\"]/input");
    public static By avanzar = By.xpath("//div[@class=\"pull-right next-step\"]/a");
}

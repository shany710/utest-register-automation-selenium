package UI;

import org.openqa.selenium.By;

public class Register1UI {
    
    public static By primerNombre = By.id("firstName");
    public static By apellido = By.id("lastName");
    public static By email = By.id("email");
    public static By mesCumpleanios = By.id("birthMonth");
    public static By diaCumpleanios = By.id("birthDay");
    public static By anioCumpleanios = By.id("birthYear");
    public static By lenguaje = By.xpath("//*[@id=\"languages\"]/div[1]/input");
    public static By avanzar = By.xpath("//*[@class='btn btn-blue']");
}

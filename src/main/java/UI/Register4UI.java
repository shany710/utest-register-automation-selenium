package UI;

import org.openqa.selenium.By;

public class Register4UI {
    
    public static By contrasenia = By.id("password");
    public static By confirmarContrasenia = By.id("confirmPassword");
    public static By terminosUso = By.id("termOfUse");
    public static By politicasPrivacidad = By.id("privacySetting");
    public static By btnRegister = By.id("laddaBtn");
}

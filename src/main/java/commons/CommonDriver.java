package commons;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class CommonDriver {

    private WebDriver driver;
    private int pageLoadTimeout;
    private String currentWorkingDirectory;
    private WebDriverWait espera;
      
    public CommonDriver(final String browserType){
        
        pageLoadTimeout = 20;
        currentWorkingDirectory = System.getProperty("user.dir");
  
        if(browserType.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", currentWorkingDirectory + "/drivers/chromedriver.exe");
            driver = new ChromeDriver();
            
        }
        
        espera = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
    }

  
    public void navigateTo(String url) {
        
        driver.manage().timeouts().pageLoadTimeout(pageLoadTimeout, TimeUnit.SECONDS);
        driver.get(url);
        
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    public void closeAllBrowser(){
        driver.quit();
    }

	public WebDriverWait getEspera() {
		return espera;
	}
    
}

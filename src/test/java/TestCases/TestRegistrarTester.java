package TestCases;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;

import Tasks.GetInRegister;
import Tasks.RegisterPage1;
import Tasks.RegisterPage2;
import Tasks.RegisterPage3;
import Tasks.RegisterPage4;
import UI.JoinTodayUI;
import UI.Register1UI;
import UI.Register2UI;
import UI.Register3UI;
import UI.Register4UI;
import commons.CommonDriver;



public class TestRegistrarTester {
	
	public CommonDriver common;
	
	@Before
	public void preSetup() {
		
		common = new CommonDriver("chrome");
		common.navigateTo("https://www.utest.com");
	}
	
	@Test
	public void registrarTesterCorrectamente() {
		
		GetInRegister.ingresarFormRegistro(common.getEspera(), JoinTodayUI.joinToday);
		
		//completar información formulario página 1
		List<String> languagesList = new ArrayList<String>();
		languagesList.add(0, "Spanish");
		
		RegisterPage1.completarInformacionForm1(common.getEspera(), 
				Register1UI.primerNombre, 
				Register1UI.apellido, 
				Register1UI.email,
				Register1UI.mesCumpleanios, 
				Register1UI.diaCumpleanios, 
				Register1UI.anioCumpleanios, 
				Register1UI.lenguaje, 
				Register1UI.avanzar, 
				"Sharon", 
				"Rojas", 
				"shany_71@hotmail.com", 
				"July", 
				"7", 
				"1996", 
				languagesList);
		
		RegisterPage2.completarInformacionForm2(common.getWebDriver(),
				common.getEspera(), 
				Register2UI.ciudad,
				Register2UI.codigoPostal, 
				Register2UI.pais, 
				Register2UI.avanzarDispositivo,
				"Barranquilla", 
				"9999", 
				"Colombia");
		
		RegisterPage3.completarInformacionForm3(common.getWebDriver(),
				common.getEspera(), 
				Register3UI.version,
				Register3UI.versionInput, 
				Register3UI.lenguaje, 
				Register3UI.lenguajeInput, 
				Register3UI.marcaDispositivo,
				Register3UI.marcaDispositivoInput, 
				Register3UI.modeloDispositivo, 
				Register3UI.modeloDispInput, 
				Register3UI.sOperativo, 
				Register3UI.sOperativoInput,
				Register3UI.avanzar, 
				"10", 
				"Spanish", 
				"Huawei",
				"P20 (Huawei)", 
				"Android 10");
		
		RegisterPage4.completarInformacionForm4(
				common.getWebDriver(),
				common.getEspera(), 
				Register4UI.contrasenia,
				Register4UI.confirmarContrasenia, 
				Register4UI.terminosUso, 
				Register4UI.politicasPrivacidad,
				Register4UI.btnRegister, 
				"Sharon*123456*", 
				"Sharon*123456*");
	}

}
